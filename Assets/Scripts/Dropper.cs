using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dropper : MonoBehaviour
{
    [SerializeField] float WaitTimer = 5f;
    private Rigidbody _rigidbody;
    private MeshRenderer _meshrenderer;
    // Start is called before the first frame update
    void Start()
    {  
        _rigidbody = gameObject.GetComponent<Rigidbody>();
        _meshrenderer = gameObject.GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if(Time.time<WaitTimer)
        {
            _rigidbody.useGravity = false;
            _meshrenderer.enabled = false;
        }
        else 
        {
            _rigidbody.useGravity = true;
            _meshrenderer.enabled = true;
        }   
    }
}
